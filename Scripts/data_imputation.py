import pandas as pd
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import KFold
from sklearn.metrics import r2_score
from utils import get_na_cols

'''
In this file, we use the technique of Regression Imputation to impute values for attributes
that are numerical in nature.

@METHODOLOGY: 
The imputation of data to missing values can be done by fitting a curve to the data. 
The best fit curve would be curve representing {y = missing_value_column and x = column_with_no_missing_value}.

That is, we fix missing values by computing a missing feature as a linear combination of other features in the data,
weighted by how correlated they are. That simply boils down to small regression problems, where:
1. missing data is our target variable 
2. existing columns with complete data as our predictor variables

We use sklearn.linear_model to learn a linear function. We make sure of the effectiveness of this approach using K-fold cross validation.
'''

def fill_missing_values(df, colname):
    
    #Fit a linear model
    X = df[df[colname].notnull()].loc[:, cols_no_na].values
    y = df[df[colname].notnull()][colname].values
    missing_value_indices = list(df[df[colname].isnull()].loc[:,:].index)

    np.random.seed(42)
    kf = KFold(n_splits=2)

    scores = []
    for train_index, test_index in kf.split(X):
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]

        clf = LinearRegression()
        clf.fit(X_train, y_train)

        y_test_pred = clf.predict(X_test)
        scores.append(r2_score(y_test, y_test_pred))
    
    print("Fit to data..")
    print(f"r2 scores for {colname} : {scores}\n")
    
    #Predict and impute
    for i in missing_value_indices:
        x = df[df[colname].isnull()].loc[i, cols_no_na].values
        df.at[i, colname] = clf.predict([x])[0]
    
    assert any(df[colname].isnull())==False

    return df


if __name__ == "__main__":
    df = pd.read_excel("../Data/Sepsis_Cleaned.xlsx")
    print("No. of columns in dataframe:", len(df.columns))

    numeric_columns = df.select_dtypes(include = ['number']).columns.tolist()
    print("No. of numeric columns in dataframe:", len(numeric_columns))

    cols_with_na = get_na_cols(df, numeric_columns) #get numeric cols with NA values
    cols_no_na = list(set(numeric_columns)-set(cols_with_na)) #get numeric cols that do not have NA

    for colname in cols_with_na:
        df = fill_missing_values(df, colname)
    
    df.to_excel("../Data/Sepsis_Imputed.xlsx", index=False)
    print("No. of attributes post imputation:", len(df.columns))
